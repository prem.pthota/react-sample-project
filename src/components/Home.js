import React, { Component } from 'react';
import { ExcelRenderer } from 'react-excel-renderer';
import { Col, Input, InputGroup, InputGroupText, FormGroup, Label, Button, Fade, FormFeedback, Container } from 'reactstrap';
import ReactTable from './ReactTable';
class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpen: false,
			dataLoaded: false,
			isFormInvalid: false,
			rows: null,
			cols: null
		}
		this.fileHandler = this.fileHandler.bind(this);
		this.toggle = this.toggle.bind(this);
		this.openFileBrowser = this.openFileBrowser.bind(this);
		this.renderFile = this.renderFile.bind(this);
		this.fileInput = React.createRef();
	}

	renderFile = (fileObj) => {
		//just pass the fileObj as parameter
		ExcelRenderer(fileObj, (err, resp) => {
			if (err) {
				alert(err)
			}
			else {
				const cols = [];
				const rows = [];
				const key = [...resp.rows].shift();

				for (let col of key) {
					cols.push({ dataField: col, text: col, sort: true })
				}
				const [, ...second] = resp.rows;
				for (let row of second) {
					var result = row.reduce(function(result, field, index) {
						result[key[index]] = field;
						return result;
					}, {})
					rows.push(result)
				}
				this.setState({
					dataLoaded: true,
					cols,
					rows
				});
			}
		});
	}

	fileHandler = (event) => {
		if (event.target.files.length) {
			let fileObj = event.target.files[0];
			let fileName = fileObj.name;


			//check for file extension and pass only if it is .xlsx and display error message otherwise
			if (fileName.slice(fileName.lastIndexOf('.') + 1) === "xlsx") {
				this.setState({
					uploadedFileName: fileName,
					isFormInvalid: false
				});
				this.renderFile(fileObj)
			}
			else {
				this.setState({
					isFormInvalid: true,
					uploadedFileName: "",
					dataLoaded: false,
					cols: null,
					rows: null
				})
			}
		}
	}

	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	openFileBrowser = () => {
		this.fileInput.current.click();
	}

	render() {
		return (
			<div>
				<Container>
					<form>
						<FormGroup row className="mt-3">
							<Label for="exampleFile" xs={6} sm={4} lg={2} size="lg">Upload Xlsx File :</Label>
							<Col xs={4} sm={8} lg={10}>
								<InputGroup>
									<Input type="text" className="form-control" value={this.state.uploadedFileName} readOnly invalid={this.state.isFormInvalid} />
									<InputGroupText>
										<Button color="info" style={{ color: "white", zIndex: 0 }} onClick={this.openFileBrowser.bind(this)}><i className="cui-file"></i> Browse&hellip;</Button>
										<input type="file" hidden onChange={this.fileHandler.bind(this)} ref={this.fileInput} onClick={(event) => { event.target.value = null }} style={{ "padding": "10px" }} />
									</InputGroupText>
									<FormFeedback>
										<Fade in={this.state.isFormInvalid} tag="h6" style={{ fontStyle: "italic" }}>
											Please select a .xlsx file only !
                  </Fade>
									</FormFeedback>
								</InputGroup>
							</Col>
						</FormGroup>
					</form>
					{this.state.dataLoaded &&
						<ReactTable columns={this.state.cols} data={this.state.rows}
						/>
					}
				</Container>
			</div>
		);
	}
}

export default Home;