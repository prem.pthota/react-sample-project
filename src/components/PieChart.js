import React, { Component } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { Col, FormGroup, Container } from 'reactstrap';
ChartJS.register(ArcElement, Tooltip, Legend);

export const data = {
	labels: ['ListPrice', 'UnitPrice', 'OrderQty', 'StandardCost'],
	datasets: [
		{
			label: '# of Price',
			data: [9.5, 5, 6, 3.3],
			backgroundColor: [
				'rgba(255, 99, 132, 0.2)',
				'rgba(54, 162, 235, 0.2)',
				'rgba(255, 206, 86, 0.2)',
				'rgba(75, 192, 192, 0.2)',
			],
			borderColor: [
				'rgba(255, 99, 132, 1)',
				'rgba(54, 162, 235, 1)',
				'rgba(255, 206, 86, 1)',
				'rgba(75, 192, 192, 1)',
			],
			borderWidth: 1,
		},
	],
};


class PieChart extends Component {
	constructor(props) { super(props) }

	render() {
		return (
			<div>
				<Container>
					<form>
						<FormGroup row>
							<Col xs={2} sm={3} lg={3} />
							<Col xs={6} sm={6} lg={6}>
								<Pie data={data} />
							</Col>
							<Col xs={2} sm={3} lg={3} />
						</FormGroup>
					</form>
				</Container>
			</div>
		)

	}
}
export default PieChart