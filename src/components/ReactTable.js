import React, { Component } from "react";
import $ from "jquery";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit';

class Table extends Component {
	constructor(props) {
		super(props)
	}
	componentDidMount() {
		$(".pagination").css("float", "right");
	}

	render() {
		const { SearchBar } = Search;

		return (
			<ToolkitProvider
				bootstrap4
				keyField="id"
				data={this.props.data}
				columns={this.props.columns}
				search
			>
				{
					props => (
						<div>
							<SearchBar {...props.searchProps} />
							<hr />
							<BootstrapTable
								{...props.baseProps}
								pagination={paginationFactory({ sizePerPage: 10 })}
							/>
						</div>
					)
				}
			</ToolkitProvider>
		)
	}
}
export default Table;