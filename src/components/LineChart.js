import React, { Component } from 'react';
import { Col, FormGroup, Container } from 'reactstrap';
import { Chart as ChartJS ,registerables} from 'chart.js';
import { Line } from "react-chartjs-2";
ChartJS.register(...registerables);


const data = {
  labels: ['ListPrice', 'UnitPrice', 'OrderQty', 'StandardCost'],
  datasets: [
    {
      label: "First dataset",
      data: [33, 53, 85, 41],
      fill: true,
      backgroundColor: "rgba(75,192,192,0.2)",
      borderColor: "rgba(75,192,192,1)"
    },
    {
      label: "Second dataset",
      data: [33, 25, 35, 51],
      fill: false,
      borderColor: "#742774"
    }
  ]
};
class LineChart extends Component {
	constructor(props) { super(props) }

	render() {
		return (
			<div><Container>
				<form>
					<FormGroup row>
						<Col xs={2} sm={3} lg={3} />
						<Col xs={6} sm={6} lg={6}>
							 <Line data={data} />
						</Col>
						<Col xs={2} sm={3} lg={3} />
					</FormGroup>
				</form>
			</Container>
			</div>)

	}
}
export default LineChart;
