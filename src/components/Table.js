import React, { Component } from 'react';
import { ExcelRenderer } from 'react-excel-renderer';
import { ToastContainer, toast } from 'react-toastify';
import { Col, Input, InputGroup, InputGroupText, FormGroup, Label, Button, Fade, FormFeedback, Container } from 'reactstrap';
import ReactTable from './ReactTable';
const selectedCols = [];
const selectedColData=[];
class Table extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpen: false,
			dataLoaded: false,
			isFormInvalid: false,
			rows: null,
			cols: null,
			rowData: null,
			colData: null,
			selectedRows: null,
			selectedCols: null,
			selectedDataLoaded: false,
		}
		this.fileHandler = this.fileHandler.bind(this);
		this.toggle = this.toggle.bind(this);
		this.openFileBrowser = this.openFileBrowser.bind(this);
		this.renderFile = this.renderFile.bind(this);
		this.fileInput = React.createRef();
	}

	renderFile = (fileObj) => {
		//just pass the fileObj as parameter
		ExcelRenderer(fileObj, (err, resp) => {
			if (err) {
				alert(err)
			}
			else {
				const cols = [];
				const rows = [];
				const key = [...resp.rows].shift();

				for (let col of key) {
					cols.push({ dataField: col, text: col, sort: true })
				}
				const [, ...second] = resp.rows;
				for (let row of second) {
					var result = row.reduce(function(result, field, index) {
						result[key[index]] = field;
						return result;
					}, {})
					rows.push(result)
				}
				this.setState({
					dataLoaded: true,
					cols,
					rows,
					rowData: rows,
					colData: cols,
				});
			}
		});
	}

	fileHandler = (event) => {
		if (event.target.files.length) {
			let fileObj = event.target.files[0];
			let fileName = fileObj.name;
			while (selectedColData.length > 0) {
				selectedColData.pop();
			};
			while (selectedCols.length > 0) {
				selectedCols.pop();
			};
			//check for file extension and pass only if it is .xlsx and display error message otherwise
			if (fileName.slice(fileName.lastIndexOf('.') + 1) === "xlsx") {
				this.setState({
					uploadedFileName: fileName,
					isFormInvalid: false
				});
				this.renderFile(fileObj)
			}
			else {
				this.setState({
					isFormInvalid: true,
					uploadedFileName: "",
					dataLoaded: false,
					cols: null,
					rows: null,
					rowData: null,
					colData: null,
					selectedRows: null,
					selectedCols: null,
					selectedDataLoaded: false,
				})
			}
		}
	}

	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	openFileBrowser = () => {
		this.fileInput.current.click();
	}

	tableHandler = (id, e) => {
		const { innerText } = e.target;
		const { rowData, colData } = this.state;
		const selectedCol = colData.find(({ text }) => text === innerText);

		if (selectedCol) {
			selectedCols.push(innerText)
			selectedColData.push(selectedCol)
			const selectedRows = []
			
			for (let value of rowData) {
				var pick = require('lodash.pick');
				let data = pick(value, selectedCols);
				selectedRows.push(data)
			}
			//console.dir(selectedRows)
			
			this.setState({
				colData: colData.filter(res => res.text != innerText),
				selectedCols:selectedColData,
				selectedRows,
				selectedDataLoaded: true
			})
		} else {
			toast.error("All ready draggable .");
		}
	}

	render() {
		return (
			<div>
				<Container>
					<form>
						<FormGroup row className="mt-3">
							<Label for="exampleFile" xs={6} sm={4} lg={2} size="lg">Upload Xlsx File :</Label>
							<Col xs={4} sm={8} lg={10}>
								<InputGroup>
									<Input type="text" className="form-control" value={this.state.uploadedFileName} readOnly invalid={this.state.isFormInvalid} />
									<InputGroupText>
										<Button color="info" style={{ color: "white", zIndex: 0 }} onClick={this.openFileBrowser.bind(this)}><i className="cui-file"></i> Browse&hellip;</Button>
										<input type="file" hidden onChange={this.fileHandler.bind(this)} ref={this.fileInput} onClick={(event) => { event.target.value = null }} style={{ "padding": "10px" }} />
									</InputGroupText>
									<FormFeedback>
										<Fade in={this.state.isFormInvalid} tag="h6" style={{ fontStyle: "italic" }}>
											Please select a .xlsx file only !
                  </Fade>
									</FormFeedback>
								</InputGroup>
							</Col>
						</FormGroup>
					</form>
					{this.state.dataLoaded &&
						<FormGroup row className="mt-3">
							<Col xs={2} sm={2} lg={2}>
								<InputGroupText>
									<ul>
										{this.state.cols.map((data, idx) => (
											<li key={idx} style={{ textAlign: "left", cursor: "pointer" }}>
												<span onClick={this.tableHandler.bind(data.text, idx)} >{data.text}</span>
											</li>

										))}
									</ul>
								</InputGroupText>
							</Col>
							<Col xs={10} sm={10} lg={10}>
								{this.state.selectedDataLoaded &&
									<ReactTable columns={this.state.selectedCols} data={this.state.selectedRows} />
								}
							</Col>
						</FormGroup>
					}
				</Container>
				<ToastContainer />
			</div>
		);
	}
}
export default Table;