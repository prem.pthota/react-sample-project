import './App.scss';
import React, { Component } from 'react';
import {
	BrowserRouter,
	Routes,
	Route,
	Link
} from "react-router-dom";
import { Col, InputGroupText, FormGroup, Container } from 'reactstrap';
import Home from './components/Home';
import Table from './components/Table';
import LineChart from './components/LineChart';
import PieChart from './components/PieChart';
class App extends Component {
	render() {
		return (
			<div>
				<Container>
					<BrowserRouter>
						<FormGroup row>
							<InputGroupText>

								<Col xs={3} sm={3} lg={3}>
									<Link to="/">Home</Link>
								</Col>
								<Col xs={3} sm={3} lg={3}>
									<Link to="/table">Table</Link>
								</Col>
								<Col xs={3} sm={3} lg={3}>
									<Link to="/line">LINE CHART</Link>

								</Col>
								<Col xs={3} sm={3} lg={3}>
									<Link to="/pie">PIE CHART</Link>

								</Col>

							</InputGroupText>
						</FormGroup>
						<Routes>
							<Route path='/' element={<Home />} />
							<Route path='/table' element={<Table />} />
							<Route path='/line' element={<LineChart />} />
							<Route path='/pie' element={<PieChart />} />
						</Routes>
					</BrowserRouter>
				</Container>
			</div>

		);
	}
}
export default App;